# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'location_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.addoredit_location_label = QtWidgets.QLabel(Dialog)
        self.addoredit_location_label.setObjectName("addoredit_location_label")
        self.verticalLayout.addWidget(self.addoredit_location_label)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.locations_list = QtWidgets.QListWidget(Dialog)
        self.locations_list.setObjectName("locations_list")
        self.horizontalLayout_2.addWidget(self.locations_list)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.addnew_radio = QtWidgets.QRadioButton(Dialog)
        self.addnew_radio.setChecked(True)
        self.addnew_radio.setObjectName("addnew_radio")
        self.verticalLayout_2.addWidget(self.addnew_radio)
        self.edit_radio = QtWidgets.QRadioButton(Dialog)
        self.edit_radio.setObjectName("edit_radio")
        self.verticalLayout_2.addWidget(self.edit_radio)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.populatedata_push = QtWidgets.QPushButton(Dialog)
        self.populatedata_push.setEnabled(False)
        self.populatedata_push.setAutoDefault(False)
        self.populatedata_push.setObjectName("populatedata_push")
        self.verticalLayout_2.addWidget(self.populatedata_push)
        self.submit_push = QtWidgets.QPushButton(Dialog)
        self.submit_push.setAutoDefault(False)
        self.submit_push.setObjectName("submit_push")
        self.verticalLayout_2.addWidget(self.submit_push)
        self.horizontalLayout_2.addLayout(self.verticalLayout_2)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.location_lineedit = QtWidgets.QLineEdit(Dialog)
        self.location_lineedit.setText("")
        self.location_lineedit.setObjectName("location_lineedit")
        self.horizontalLayout.addWidget(self.location_lineedit)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.addoredit_location_label.setText(_translate("Dialog", "Add or Edit Location:"))
        self.addnew_radio.setText(_translate("Dialog", "Add New"))
        self.edit_radio.setText(_translate("Dialog", "Edit Existing"))
        self.populatedata_push.setText(_translate("Dialog", "Populate Data"))
        self.submit_push.setText(_translate("Dialog", "Submit"))
        self.location_lineedit.setPlaceholderText(_translate("Dialog", "Location"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
