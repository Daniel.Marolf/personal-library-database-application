Welcome to the Personal Library Database Application!

This application was created using Python and SQL. For the GUI, I used a python module called PyQt5. For the background database, I used MySQL. Then, to connect the two I used another python module called mysql-connector-python.

In order to set up this application, you will first need to install MySQL and run the 'library_database_setup.sql' script. This script will create a database called "library", create all of the necessary tables, and define the table relationships. 

OPTIONALLY: If you want to see how this works before adding all of your own books, you can run the upload_sample_books.sql script. This will add a random assortment of books that I selected from popular book lists on the internet to your database. If you want to restart and add just your own books afterward, run "DROP DATABASE library;" in the MySQL Command Line Client. Then, run the 'library_database_setup.sql' script again.

To start the application, run the 'library_main.py' script. Each of the other .py files was created using Qt Designer, a program intended to help streamline the creation of PyQt windows, dialogs, and applications. Each of those .py files is named according to what it does and is imported into the 'library_main.py' script where necessary. These files can obviously be edited, but in my experience, the best way to edit them is to load the .ui files into Qt Designer and edit them there. The reason is that when you convert from a .ui file to a .py, it will overwrite your manual changes. (See the following video for more discussion of this - "[QtDesigner and PyQt5: The right and wrong way to use them together](https://www.youtube.com/watch?v=XXPNpdaK9WA&t=1865s)"). You can find .ui files for each of the dialog windows and for the main library in the "Qt Designer ui files" folder.

If there are any features or improvements I should make, message me on my linkedin page: https://www.linkedin.com/in/daniel-marolf-a6515a41/. This has been a fun project for me, so I really hope you enjoy it! 
