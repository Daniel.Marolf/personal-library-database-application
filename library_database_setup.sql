CREATE DATABASE library;
USE library;

CREATE TABLE keywords (
	keyword_id int NOT NULL auto_increment,
    keyword varchar(255) NOT NULL,
    PRIMARY KEY (keyword_id)
);

CREATE TABLE authors (
	author_id int NOT NULL auto_increment,
    last_name varchar(255) NOT NULL,
    first_name varchar(255) NOT NULL,
    PRIMARY KEY (author_id)
);

CREATE TABLE editors (
editor_id int NOT NULL auto_increment,
last_name varchar(255) NOT NULL,
first_name varchar(255) NOT NULL, 
PRIMARY KEY (editor_id)
);

CREATE TABLE locations (
location_id int NOT NULL auto_increment,
location varchar(255) NOT NULL,
PRIMARY KEY (location_id)
);

CREATE TABLE books (
	book_id int NOT NULL auto_increment,
    title varchar(255) NOT NULL,
    has_author bool NOT NULL,
    has_editor bool NOT NULL,
    location_id int NOT NULL,
    PRIMARY KEY (book_id),
    FOREIGN KEY (location_id) REFERENCES locations(location_id)
);

CREATE TABLE authors_books (
	book_id int NOT NULL,
    author_id int NOT NULL,
    FOREIGN KEY (book_id) REFERENCES books(book_id),
    FOREIGN KEY (author_id) REFERENCES authors(author_id)
);

CREATE TABLE editors_books (
	book_id int NOT NULL,
    editor_id int NOT NULL,
    FOREIGN KEY (book_id) REFERENCES books(book_id),
    FOREIGN KEY (editor_id) REFERENCES editors(editor_id)
);

CREATE TABLE keywords_books (
	book_id int NOT NULL,
    keyword_id int NOT NULL,
    FOREIGN KEY (book_id) REFERENCES books(book_id),
    FOREIGN KEY (keyword_id) REFERENCES keywords(keyword_id)
)

