"""
To Do List
- figure out why it works in python 3.8, but not 3.9
- create a button/function for adding a new keyword to multiple books
- crashes when editing editor name twice in a row without populating data again
- would like the authors, editors, and keywords to appear in alphabetical order in the results table
- add error messages throughout
    - add an error message for if a title, author, editor, or keyword contains the " character. It will not work.
    Will need to use ' instead. (Or supply an escape character).
- maybe add a typing section on the author's, editor's, and keywords sections and have the ui filter as you type.

"""

import os
from library import Ui_MainWindow
from author_dialog import Ui_Dialog as author_Ui_Dialog
from editor_dialog import Ui_Dialog as editor_Ui_Dialog
from keyword_dialog import Ui_Dialog as keyword_Ui_Dialog
from location_dialog import Ui_Dialog as location_Ui_Dialog
import mysql.connector
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui

db_user = os.environ.get('DB_USER') # username and password saved in environment variables
db_password = os.environ.get('DB_PASS')

db = mysql.connector.connect(user=db_user,
                             password=db_password,
                             host='127.0.0.1',
                             database='library',)
                             # auth_plugin='mysql_native_password')

mycursor = db.cursor()


class LibraryWindow(qtw.QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.setWindowTitle('Library App')

        self.ui.results_table.verticalHeader().setVisible(False)

        self.update_results()
        self.update_locations()
        self.update_authors()
        self.update_editors()
        self.update_keywords()

        self.ui.authors_tool.clicked.connect(self.launch_author_dialog)
        self.ui.editors_tool.clicked.connect(self.launch_editor_dialog)
        self.ui.keywords_tool.clicked.connect(self.launch_keyword_dialog)
        self.ui.locations_tool.clicked.connect(self.launch_location_dialog)
        self.ui.editbook_check.clicked.connect(self.edit_book_toggle)
        self.ui.populatedata_push.clicked.connect(self.populate_data)
        self.ui.submit_push.clicked.connect(self.submit_book)
        self.ui.search_push.clicked.connect(self.update_results)
        self.ui.addfilter_push.clicked.connect(self.add_filter)
        self.ui.deletefilter_push.clicked.connect(self.delete_filter)

    def closeEvent(self, a0: QtGui.QCloseEvent):
        db.close()

    def update_locations(self):
        self.ui.location_combo.clear()
        mycursor.execute("SELECT location FROM locations")
        all_locations = mycursor.fetchall()
        for i in all_locations:
            self.ui.location_combo.addItem(i[0])

    def update_authors(self):
        self.ui.authors_list.clear()
        mycursor.execute("SELECT * FROM authors ORDER BY last_name ASC")
        all_authors = mycursor.fetchall()
        for i in all_authors:
            item = qtw.QListWidgetItem()
            self.ui.authors_list.addItem(item)
            author = ', '.join(i[1:])
            item.setText(author)

    def update_editors(self):
        self.ui.editors_list.clear()
        mycursor.execute("SELECT * FROM editors ORDER BY last_name ASC")
        all_editors = mycursor.fetchall()
        for i in all_editors:
            item = qtw.QListWidgetItem()
            self.ui.editors_list.addItem(item)
            editor = ', '.join(i[1:])
            item.setText(str(editor))

    def update_keywords(self):
        self.ui.keywords_list.clear()
        mycursor.execute("SELECT * FROM keywords ORDER BY keyword ASC")
        all_keywords = mycursor.fetchall()
        for i in all_keywords:
            item = qtw.QListWidgetItem()
            self.ui.keywords_list.addItem(item)
            item.setText(i[1])

    def add_filter(self):
        filter_type = self.ui.filter_combo.currentText()
        filter_value = self.ui.filter_lineedit.text().strip().title()
        filter = f"{filter_type}= '{filter_value}'"
        item = qtw.QListWidgetItem()
        self.ui.filters_list.addItem(item)
        item.setText(filter)
        self.ui.filter_lineedit.clear()

    def delete_filter(self):
        filters = self.ui.filters_list.selectedItems()
        for i in filters:
            item_row = self.ui.filters_list.row(i)
            self.ui.filters_list.takeItem(item_row)

    def filter_books(self):
        filter_count = self.ui.filters_list.count()
        filters = []
        for i in range(filter_count):
            filter = self.ui.filters_list.item(i)
            filter = filter.text()
            filters.append(filter)
        if len(filters) > 0:
            all_books = []
            for i in filters:
                filter_type, filter_value = i.split('= ')
                filter_value = filter_value.strip("'")
                if filter_type == 'Title (or partial title)':
                    mycursor.execute(f'SELECT book_id FROM books WHERE title LIKE "%{filter_value}%"')
                    books = mycursor.fetchall()
                    if len(all_books) > 0:
                        all_books_TEMP = all_books[:]
                        for j in all_books:
                            if j not in books:
                                all_books_TEMP.remove(j)
                        all_books = all_books_TEMP
                    else:
                        for j in books:
                            all_books.append(j)
                if filter_type == 'Keyword':
                    mycursor.execute(f'SELECT keyword_id FROM keywords WHERE keyword="{filter_value}"')
                    keyword_id = mycursor.fetchall()[0][0]
                    mycursor.execute(f"SELECT book_id FROM keywords_books WHERE keyword_id={keyword_id}")
                    books = mycursor.fetchall()
                    if len(all_books) > 0:
                        all_books_TEMP = all_books[:]
                        for j in all_books:
                            if j not in books:
                                all_books_TEMP.remove(j)
                        all_books = all_books_TEMP
                    else:
                        for j in books:
                            all_books.append(j)
                if filter_type == 'Author (last name)':
                    mycursor.execute(f'SELECT author_id FROM authors WHERE last_name="{filter_value}"')
                    author_ids = mycursor.fetchall()[0]
                    for j in author_ids:
                        mycursor.execute(f"SELECT book_id FROM authors_books WHERE author_id={j}")
                        books = mycursor.fetchall()
                        if len(all_books) > 0:
                            all_books_TEMP = all_books[:]
                            for k in all_books:
                                if k not in books:
                                    all_books_TEMP.remove(k)
                            all_books = all_books_TEMP
                        else:
                            for k in books:
                                all_books.append(k)
                if filter_type == 'Author (first name)':
                    mycursor.execute(f'SELECT author_id FROM authors WHERE first_name="{filter_value}"')
                    author_ids = mycursor.fetchall()[0]
                    for j in author_ids:
                        mycursor.execute(f"SELECT book_id FROM authors_books WHERE author_id={j}")
                        books = mycursor.fetchall()
                        if len(all_books) > 0:
                            all_books_TEMP = all_books[:]
                            for k in all_books:
                                if k not in books:
                                    all_books_TEMP.remove(k)
                            all_books = all_books_TEMP
                        else:
                            for k in books:
                                all_books.append(k)
                if filter_type == 'Editor (last name)':
                    mycursor.execute(f'SELECT editor_id FROM editors WHERE last_name="{filter_value}"')
                    editor_ids = mycursor.fetchall()[0]
                    for j in editor_ids:
                        mycursor.execute(f"SELECT book_id FROM editors_books WHERE editor_id={j}")
                        books = mycursor.fetchall()
                        if len(all_books) > 0:
                            all_books_TEMP = all_books[:]
                            for k in all_books:
                                if k not in books:
                                    all_books_TEMP.remove(k)
                            all_books = all_books_TEMP
                        else:
                            for k in books:
                                all_books.append(k)
                if filter_type == 'Editor (first name)':
                    mycursor.execute(f'SELECT editor_id FROM editors WHERE first_name="{filter_value}"')
                    editor_ids = mycursor.fetchall()[0]
                    for j in editor_ids:
                        mycursor.execute(f"SELECT book_id FROM editors_books WHERE editor_id={j}")
                        books = mycursor.fetchall()
                        if len(all_books) > 0:
                            all_books_TEMP = all_books[:]
                            for k in all_books:
                                if k not in books:
                                    all_books_TEMP.remove(k)
                            all_books = all_books_TEMP
                        else:
                            for k in books:
                                all_books.append(k)
                if filter_type == 'Location':
                    mycursor.execute(f'SELECT location_id FROM locations WHERE location="{filter_value}"')
                    location_id = mycursor.fetchall()[0][0]
                    mycursor.execute(f"SELECT book_id FROM books WHERE location_id={location_id}")
                    books = mycursor.fetchall()
                    if len(all_books) > 0:
                        all_books_TEMP = all_books[:]
                        for j in all_books:
                            if j not in books:
                                all_books_TEMP.remove(j)
                        all_books = all_books_TEMP
                    else:
                        for j in books:
                            all_books.append(j)
            query = 'SELECT book_id FROM books '
            condition = 'WHERE '
            for i in all_books:
                condition += f"book_id={str(i[0])} "
                if i != all_books[-1]:
                    condition += 'OR '
            if self.ui.orderby_mostrecent_radio.isChecked():
                order_by = 'ORDER BY book_id DESC'
            else:
                order_by = 'ORDER BY title ASC'
            query += condition + order_by
            return query
        else:
            if self.ui.orderby_mostrecent_radio.isChecked():
                query = "SELECT book_id FROM books ORDER BY book_id DESC"
                return query
            else:
                query = "SELECT book_id FROM books ORDER BY title ASC"
                return query

    def update_results(self):
        self.ui.results_table.clear()
        table_headers = ['Book ID#','Title','Author(s)','Editor(s)','Location','Keyword(s)']
        for i in table_headers:
            item = qtw.QTableWidgetItem()
            self.ui.results_table.setHorizontalHeaderItem(table_headers.index(i), item)
            item.setText(i)
        query = self.filter_books()
        mycursor.execute(query)
        all_books = mycursor.fetchall()
        self.ui.results_table.setRowCount(len(all_books))
        self.ui.results_label.setText(f'Results: {str(len(all_books))}')
        x = 0
        for book in all_books:
            mycursor.execute(f"SELECT title FROM books WHERE book_id={book[0]}")
            title = mycursor.fetchall()[0][0]
            mycursor.execute(f"SELECT location_id FROM books WHERE book_id={book[0]}")
            location_id = mycursor.fetchall()[0][0]
            mycursor.execute(f"SELECT location FROM locations WHERE location_id={location_id}")
            location = mycursor.fetchall()[0][0]
            mycursor.execute(f"SELECT has_author FROM books WHERE book_id={book[0]}")
            has_author = mycursor.fetchall()[0][0]
            if has_author == 1:
                mycursor.execute(f"SELECT author_id FROM authors_books WHERE book_id={book[0]}")
                all_author_ids = mycursor.fetchall()
                all_authors = []
                for author_id in all_author_ids:
                    mycursor.execute(f"SELECT last_name, first_name FROM authors "
                                     f"WHERE author_id={author_id[0]}")
                    last_name, first_name = mycursor.fetchall()[0]
                    author = last_name + ', ' + first_name
                    all_authors.append(author)
                authors = '\n'.join(all_authors)
            else:
                authors = ''
            mycursor.execute(f"SELECT has_editor FROM books WHERE book_id={book[0]}")
            has_editor = mycursor.fetchall()[0][0]
            if has_editor == 1:
                mycursor.execute(f"SELECT editor_id FROM editors_books WHERE book_id={book[0]}")
                all_editor_ids = mycursor.fetchall()
                all_editors = []
                for editor_id in all_editor_ids:
                    mycursor.execute(f"SELECT last_name, first_name FROM editors "
                                     f"WHERE editor_id={editor_id[0]}")
                    last_name, first_name = mycursor.fetchall()[0]
                    editor = last_name + ', ' + first_name
                    all_editors.append(editor)
                editors = '\n'.join(all_editors)
            else:
                editors = ''
            mycursor.execute(f"SELECT keyword_id FROM keywords_books WHERE book_id={book[0]}")
            all_keyword_ids = mycursor.fetchall()
            if len(all_keyword_ids) > 0:
                all_keywords = []
                for keyword_id in all_keyword_ids:
                    mycursor.execute(f"SELECT keyword FROM keywords "
                                     f"WHERE keyword_id={keyword_id[0]}")
                    keyword = mycursor.fetchall()[0][0]
                    all_keywords.append(keyword)
                keywords = ',\n'.join(all_keywords)
            else:
                keywords = ''
            bookid_item = qtw.QTableWidgetItem()
            bookid_item.setText(str(book[0]))
            title_item = qtw.QTableWidgetItem()
            title_item.setText(title)
            authors_item = qtw.QTableWidgetItem()
            authors_item.setText(authors)
            editors_item = qtw.QTableWidgetItem()
            editors_item.setText(editors)
            location_item = qtw.QTableWidgetItem()
            location_item.setText(location)
            keywords_item = qtw.QTableWidgetItem()
            keywords_item.setText(keywords)
            self.ui.results_table.setItem(x, 0, bookid_item) # .setItem(row, col, item)
            self.ui.results_table.setItem(x, 1, title_item)
            self.ui.results_table.setItem(x, 2, authors_item)
            self.ui.results_table.setItem(x, 3, editors_item)
            self.ui.results_table.setItem(x, 4, location_item)
            self.ui.results_table.setItem(x, 5, keywords_item)
            x+=1
        self.ui.results_table.resizeRowsToContents()
        self.ui.results_table.resizeColumnToContents(0) # this looks stupid, but titles can be really long and none of the other columns are going to struggle with that.
        self.ui.results_table.resizeColumnToContents(2) # I want that column to resize to the rows, but the rest of them to resize to the contents.
        self.ui.results_table.resizeColumnToContents(3)
        self.ui.results_table.resizeColumnToContents(4)
        self.ui.results_table.resizeColumnToContents(6)

    def submit_book(self):
        # Editing a book
        if self.ui.editbook_check.isChecked():
            title = self.ui.title_lineedit.text().strip().title()
            location = self.ui.location_combo.currentText()
            mycursor.execute(f'SELECT location_id FROM locations WHERE location="{location}"')
            location_id = mycursor.fetchall()[0][0]
            authors_list = self.ui.authors_list.selectedItems()
            if len(authors_list) > 0:
                has_author = 1
            else:
                has_author = 0
            editors_list = self.ui.editors_list.selectedItems()
            if len(editors_list) > 0:
                has_editor = 1
            else:
                has_editor = 0
            book_id = self.ui.bookid_lineedit.text()
            mycursor.execute(f'UPDATE books '
                             f'SET title="{title}", has_author={has_author}, has_editor={has_editor}, '
                             f'location_id={location_id} '
                             f'WHERE book_id={book_id}')
            db.commit()
            # Authors
            mycursor.execute(f"SELECT author_id FROM authors_books WHERE book_id={book_id}")
            current_authors_TEMP = mycursor.fetchall()
            current_authors = []
            for i in current_authors_TEMP:
                current_authors.append(i[0])
            for author in authors_list:
                last_name, first_name = author.text().split(', ')
                mycursor.execute(f'SELECT author_id FROM authors '
                                 f'WHERE (last_name="{last_name}" AND first_name="{first_name}")')
                author_id = mycursor.fetchall()[0][0]
                if author_id not in current_authors:
                    mycursor.execute(f"INSERT INTO authors_books (book_id, author_id) "
                                     f"VALUES ({book_id}, {author_id})")
                    db.commit()
                else:
                    current_authors.remove(author_id)
            if len(current_authors) > 0: # this would mean that there are some authors to be removed
                for i in current_authors:
                    mycursor.execute(f"DELETE FROM authors_books WHERE book_id={book_id} AND author_id={i}")
                    db.commit()
            # Editors
            mycursor.execute(f"SELECT editor_id FROM editors_books WHERE book_id={book_id}")
            current_editors_TEMP = mycursor.fetchall()
            current_editors = []
            for i in current_editors_TEMP:
                current_editors.append(i[0])
            for editor in editors_list:
                last_name, first_name = editor.text().split(', ')
                mycursor.execute(f'SELECT editor_id FROM editors '
                                 f'WHERE (last_name="{last_name}" AND first_name="{first_name}")')
                editor_id = mycursor.fetchall()[0][0]
                if editor_id not in current_editors:
                    mycursor.execute(f"INSERT INTO editors_books (book_id, editor_id) "
                                     f"VALUES ({book_id}, {editor_id})")
                    db.commit()
                else:
                    current_editors.remove(editor_id)
            if len(current_editors) > 0:  # this would mean that there are some editors to be removed
                for i in current_editors:
                    mycursor.execute(f"DELETE FROM editors_books WHERE book_id={book_id} AND editor_id={i}")
                    db.commit()
            # Keywords
            keywords_list = self.ui.keywords_list.selectedItems()
            mycursor.execute(f"SELECT keyword_id FROM keywords_books WHERE book_id={book_id}")
            current_keywords_TEMP = mycursor.fetchall()
            current_keywords = []
            for i in current_keywords_TEMP:
                current_keywords.append(i[0])
            for keyword in keywords_list:
                keyword = keyword.text()
                mycursor.execute(f'SELECT keyword_id FROM keywords WHERE keyword="{keyword}"')
                keyword_id = mycursor.fetchall()[0][0]
                if keyword_id not in current_keywords:
                    mycursor.execute(f"INSERT INTO keywords_books (book_id, keyword_id) "
                                     f"VALUES ({book_id}, {keyword_id})")
                    db.commit()
                else:
                    current_keywords.remove(keyword_id) # keyword already associated with book and needs to remain
            if len(current_keywords) > 0:
                for i in current_keywords:
                    mycursor.execute(f"DELETE FROM keywords_books WHERE book_id={book_id} and keyword_id={i}")
                    db.commit()
        # Adding a new book
        else:
            title = self.ui.title_lineedit.text().strip().title()
            location = self.ui.location_combo.currentText()
            mycursor.execute(f'SELECT location_id FROM locations WHERE location="{location}"')
            location_id = mycursor.fetchall()[0][0]
            authors_list = self.ui.authors_list.selectedItems()
            if len(authors_list) > 0:
                has_author = 1
            else:
                has_author = 0
            editors_list = self.ui.editors_list.selectedItems()
            if len(editors_list) > 0:
                has_editor = 1
            else:
                has_editor = 0
            mycursor.execute(f'INSERT INTO books (title, has_author, has_editor, location_id) '
                             f'VALUES ("{title}", {has_author}, {has_editor}, {location_id})')
            db.commit()
            mycursor.execute("SELECT book_id FROM books ORDER BY book_id DESC LIMIT 1")
            book_id = mycursor.fetchall()[0][0]
            # Authors
            for author in authors_list:
                last_name, first_name = author.text().split(', ')
                mycursor.execute(f'SELECT author_id FROM authors '
                                 f'WHERE (last_name="{last_name}" AND first_name="{first_name}")')
                author_id = mycursor.fetchall()[0][0]
                if self.ui.editbook_check.isChecked():
                    mycursor.execute(f"UPDATE authors_books SET ")
                else:
                    mycursor.execute(f"INSERT INTO authors_books (book_id, author_id) VALUES ({book_id},{author_id})")
                    db.commit()
            # Editors
            for editor in editors_list:
                last_name, first_name = editor.text().split(', ')
                mycursor.execute(f'SELECT editor_id FROM editors '
                                 f'WHERE (last_name="{last_name}" AND first_name="{first_name}")')
                editor_id = mycursor.fetchall()[0][0]
                mycursor.execute(f"INSERT INTO editors_books (book_id, editor_id) VALUES ({book_id},{editor_id})")
                db.commit()
            # Keywords
            keywords_list = self.ui.keywords_list.selectedItems()
            for keyword in keywords_list:
                keyword = keyword.text()
                mycursor.execute(f'SELECT keyword_id FROM keywords WHERE keyword="{keyword}"')
                keyword_id = mycursor.fetchall()[0][0]
                mycursor.execute(f"INSERT INTO keywords_books (book_id, keyword_id) VALUES ({book_id}, {keyword_id})")
                db.commit()
        self.update_results()
        self.clear_entries()
        self.ui.editbook_check.setChecked(False)

    def clear_entries(self):
        self.ui.title_lineedit.clear()
        mycursor.execute('SELECT location FROM locations ORDER BY location_id ASC LIMIT 1')
        location = mycursor.fetchall()[0][0]
        self.ui.location_combo.setCurrentText(location)
        self.ui.bookid_lineedit.clear()
        selected_authors = self.ui.authors_list.selectedItems()
        for i in selected_authors:
            i.setSelected(False)
        selected_editors = self.ui.editors_list.selectedItems()
        for i in selected_editors:
            i.setSelected(False)
        selected_keywords = self.ui.keywords_list.selectedItems()
        for i in selected_keywords:
            i.setSelected(False)

    def edit_book_toggle(self):
        if self.ui.editbook_check.isChecked():
            self.ui.populatedata_push.setEnabled(True)
            self.ui.bookid_lineedit.setEnabled(True)
        else:
            self.ui.populatedata_push.setEnabled(False)
            self.ui.bookid_lineedit.setEnabled(False)

    def populate_data(self):
        book_id = int(self.ui.bookid_lineedit.text())
        mycursor.execute(f"SELECT * FROM books WHERE book_id={book_id}")
        book = mycursor.fetchall()
        title = book[0][1]
        self.ui.title_lineedit.setText(title)
        location_id = book[0][4]
        mycursor.execute(f"SELECT location FROM locations WHERE location_id={location_id}")
        location = mycursor.fetchall()[0][0]
        self.ui.location_combo.setCurrentText(location)
        mycursor.execute(f"SELECT author_id FROM authors_books WHERE book_id={book_id}")
        author_id = mycursor.fetchall()
        if len(author_id) > 0:
            all_authors = []
            for i in author_id:
                mycursor.execute(f"SELECT last_name, first_name FROM authors "
                                 f"WHERE author_id={i[0]}")
                last_name, first_name = mycursor.fetchall()[0]
                author = last_name + ', ' + first_name
                all_authors.append(author)
            for i in all_authors:
                item = self.ui.authors_list.findItems(i, qtc.Qt.MatchFlag.MatchExactly)[0]
                item.setSelected(True)
        mycursor.execute(f"SELECT editor_id FROM editors_books WHERE book_id={book_id}")
        editor_id = mycursor.fetchall()
        if len(editor_id) > 0:
            all_editors = []
            for i in editor_id:
                mycursor.execute(f"SELECT last_name, first_name FROM editors "
                                 f"WHERE editor_id={i[0]}")
                last_name, first_name = mycursor.fetchall()[0]
                editor = last_name + ', ' + first_name
                all_editors.append(editor)
            for i in all_editors:
                item = self.ui.editors_list.findItems(i, qtc.Qt.MatchFlag.MatchExactly)[0]
                item.setSelected(True)
        mycursor.execute(f"SELECT keyword_id FROM keywords_books WHERE book_id={book_id}")
        keyword_id = mycursor.fetchall()
        if len(keyword_id) > 0:
            all_keywords = []
            for i in keyword_id:
                mycursor.execute(f"SELECT keyword FROM keywords "
                                 f"WHERE keyword_id={i[0]}")
                keyword = mycursor.fetchall()[0][0]
                all_keywords.append(keyword)
            for i in all_keywords:
                item = self.ui.keywords_list.findItems(i, qtc.Qt.MatchFlag.MatchExactly)[0]
                item.setSelected(True)


    def launch_author_dialog(self):
        dlg = AuthorDialog()
        dlg.exec()
        self.update_authors()

    def launch_editor_dialog(self):
        dlg = EditorDialog()
        dlg.exec()
        self.update_editors()

    def launch_location_dialog(self):
        dlg = LocationDialog()
        dlg.exec()
        self.update_locations()
    
    def launch_keyword_dialog(self):
        dlg = KeywordDialog()
        dlg.exec()
        self.update_keywords()


class AuthorDialog(qtw.QDialog):
    def __init__(self):
        super().__init__()
        self.author_dialog = author_Ui_Dialog()
        self.author_dialog.setupUi(self)
        self.setWindowTitle('Add or Edit Author')

        self.update_authors()

        self.error_msg = qtw.QErrorMessage()
        self.error_msg.setWindowTitle('Error')

        self.author_dialog.submit_push.clicked.connect(self.submit)
        self.author_dialog.addnew_radio.clicked.connect(self.radio_toggled)
        self.author_dialog.edit_radio.clicked.connect(self.radio_toggled)
        self.author_dialog.populatedata_push.clicked.connect(self.populate_data)

    def update_authors(self):
        self.author_dialog.authors_list.clear()
        mycursor.execute("SELECT * FROM authors ORDER BY last_name ASC")
        all_authors = mycursor.fetchall()
        for i in all_authors:
            item = qtw.QListWidgetItem()
            self.author_dialog.authors_list.addItem(item)
            author = ', '.join(i[1:])
            item.setText(author)

    def radio_toggled(self):
        if self.author_dialog.addnew_radio.isChecked():
            self.author_dialog.populatedata_push.setEnabled(False)
        else:
            self.author_dialog.populatedata_push.setEnabled(True)

    def populate_data(self):
        author = self.author_dialog.authors_list.selectedItems()[0].text()
        last_name, first_name = author.split(', ')
        self.author_dialog.lastname_lineedit.setText(last_name)
        self.author_dialog.firstname_lineedit.setText(first_name)

    def submit(self):
        last_name = self.author_dialog.lastname_lineedit.text().strip().title()
        first_name = self.author_dialog.firstname_lineedit.text().strip().title()
        if last_name == '' or first_name == '':
            self.error_msg.showMessage('Both "Last Name" and "First Name" fields must be filled.')
        else:
            if self.author_dialog.addnew_radio.isChecked():
                mycursor.execute(f'INSERT INTO authors (last_name, first_name) '
                                 f'VALUES ("{last_name}","{first_name}")')
                db.commit()
                self.update_authors()
            if self.author_dialog.edit_radio.isChecked():
                editedauthor = self.author_dialog.authors_list.selectedItems()[0].text()
                old_last_name, old_first_name = editedauthor.split(', ')
                mycursor.execute(f'SELECT author_id FROM authors '
                                 f'WHERE last_name="{old_last_name}" AND first_name="{old_first_name}"')
                author_id = mycursor.fetchall()[0][0]
                mycursor.execute(f'UPDATE authors '
                                 f'SET last_name="{last_name}", first_name="{first_name}" '
                                 f'WHERE author_id={author_id}')
                db.commit()
                self.update_authors()
        self.author_dialog.lastname_lineedit.clear()
        self.author_dialog.firstname_lineedit.clear()


class EditorDialog(qtw.QDialog):
    def __init__(self):
        super().__init__()
        self.editor_dialog = editor_Ui_Dialog()
        self.editor_dialog.setupUi(self)
        self.setWindowTitle('Add or Edit Editor')

        self.update_editors()

        self.error_msg = qtw.QErrorMessage()
        self.error_msg.setWindowTitle('Error')

        self.editor_dialog.submit_push.clicked.connect(self.submit)
        self.editor_dialog.addnew_radio.clicked.connect(self.radio_toggled)
        self.editor_dialog.edit_radio.clicked.connect(self.radio_toggled)
        self.editor_dialog.populatedata_push.clicked.connect(self.populate_data)

    def update_editors(self):
        self.editor_dialog.editors_list.clear()
        mycursor.execute("SELECT * FROM editors ORDER BY last_name ASC")
        all_editors = mycursor.fetchall()
        for i in all_editors:
            item = qtw.QListWidgetItem()
            self.editor_dialog.editors_list.addItem(item)
            editor = ', '.join(i[1:])
            item.setText(editor)

    def radio_toggled(self):
        if self.editor_dialog.addnew_radio.isChecked():
            self.editor_dialog.populatedata_push.setEnabled(False)
        else:
            self.editor_dialog.populatedata_push.setEnabled(True)

    def populate_data(self):
        editor = self.editor_dialog.editors_list.selectedItems()[0].text()
        last_name, first_name = editor.split(', ')
        self.editor_dialog.lastname_lineedit.setText(last_name)
        self.editor_dialog.firstname_lineedit.setText(first_name)

    def submit(self):
        last_name = self.editor_dialog.lastname_lineedit.text().strip().title()
        first_name = self.editor_dialog.firstname_lineedit.text().strip().title()
        if last_name == '' or first_name == '':
            self.error_msg.showMessage('Both "Last Name" and "First Name" fields must be filled.')
        else:
            if self.editor_dialog.addnew_radio.isChecked():
                mycursor.execute(f'INSERT INTO editors (last_name, first_name) '
                                 f'VALUES ("{last_name}","{first_name}")')
                db.commit()
                self.update_editors()
            if self.editor_dialog.edit_radio.isChecked():
                editededitor = self.editor_dialog.editors_list.selectedItems()[0].text()
                old_last_name, old_first_name = editededitor.split(', ')
                mycursor.execute(f'SELECT editor_id FROM editors '
                                 f'WHERE last_name="{old_last_name}" AND first_name="{old_first_name}"')
                editor_id = mycursor.fetchall()[0][0]
                mycursor.execute(f'UPDATE editors '
                                 f'SET last_name="{last_name}", first_name="{first_name}"'
                                 f'WHERE editor_id={editor_id}')
                db.commit()
                self.update_editors()
        self.editor_dialog.lastname_lineedit.clear()
        self.editor_dialog.firstname_lineedit.clear()


class KeywordDialog(qtw.QDialog):
    def __init__(self):
        super().__init__()
        self.keyword_dialog = keyword_Ui_Dialog()
        self.keyword_dialog.setupUi(self)
        self.setWindowTitle('Add or Edit Keyword')

        self.update_keywords()

        self.error_msg = qtw.QErrorMessage()
        self.error_msg.setWindowTitle('Error')

        self.keyword_dialog.submit_push.clicked.connect(self.submit)
        self.keyword_dialog.addnew_radio.clicked.connect(self.radio_toggled)
        self.keyword_dialog.edit_radio.clicked.connect(self.radio_toggled)
        self.keyword_dialog.populatedata_push.clicked.connect(self.populate_data)

    def update_keywords(self):
        self.keyword_dialog.keywords_list.clear()
        mycursor.execute("SELECT * FROM keywords ORDER BY keyword ASC")
        all_keywords = mycursor.fetchall()
        for i in all_keywords:
            item = qtw.QListWidgetItem()
            self.keyword_dialog.keywords_list.addItem(item)
            item.setText(i[1])

    def radio_toggled(self):
        if self.keyword_dialog.addnew_radio.isChecked():
            self.keyword_dialog.populatedata_push.setEnabled(False)
        else:
            self.keyword_dialog.populatedata_push.setEnabled(True)

    def populate_data(self):
        keyword = self.keyword_dialog.keywords_list.selectedItems()[0].text()
        self.keyword_dialog.keyword_lineedit.setText(keyword)

    def submit(self):
        keyword = self.keyword_dialog.keyword_lineedit.text().strip().title()
        if keyword == '':
            self.error_msg.showMessage('"Keyword" field must be filled.')
        else:
            if self.keyword_dialog.addnew_radio.isChecked():
                mycursor.execute(f'INSERT INTO keywords (keyword) '
                                 f'VALUES ("{keyword}")')
                db.commit()
                self.update_keywords()
            if self.keyword_dialog.edit_radio.isChecked():
                editedkeyword = self.keyword_dialog.keywords_list.selectedItems()[0].text()
                mycursor.execute(f'SELECT keyword_id FROM keywords '
                                 f'WHERE keyword="{editedkeyword}"')
                keyword_id = mycursor.fetchall()[0][0]
                mycursor.execute(f'UPDATE keywords '
                                 f'SET keyword="{keyword}"'
                                 f'WHERE keyword_id={keyword_id}')
                db.commit()
                self.update_keywords()
        self.keyword_dialog.keyword_lineedit.clear()


class LocationDialog(qtw.QDialog):
    def __init__(self):
        super().__init__()
        self.location_dialog = location_Ui_Dialog()
        self.location_dialog.setupUi(self)
        self.setWindowTitle('Add or Edit Location')

        self.update_locations()

        self.error_msg = qtw.QErrorMessage()
        self.error_msg.setWindowTitle('Error')

        self.location_dialog.submit_push.clicked.connect(self.submit)
        self.location_dialog.addnew_radio.clicked.connect(self.radio_toggled)
        self.location_dialog.edit_radio.clicked.connect(self.radio_toggled)
        self.location_dialog.populatedata_push.clicked.connect(self.populate_data)

    def update_locations(self):
        self.location_dialog.locations_list.clear()
        mycursor.execute("SELECT * FROM locations")
        all_locations = mycursor.fetchall()
        for i in all_locations:
            item = qtw.QListWidgetItem()
            self.location_dialog.locations_list.addItem(item)
            item.setText(i[1])

    def radio_toggled(self):
        if self.location_dialog.addnew_radio.isChecked():
            self.location_dialog.populatedata_push.setEnabled(False)
        else:
            self.location_dialog.populatedata_push.setEnabled(True)

    def populate_data(self):
        location = self.location_dialog.locations_list.selectedItems()[0].text()
        self.location_dialog.location_lineedit.setText(location)

    def submit(self):
        location = self.location_dialog.location_lineedit.text().strip().title()
        if location == '':
            self.error_msg.showMessage('"Location" field must be filled.')
        else:
            if self.location_dialog.addnew_radio.isChecked():
                mycursor.execute(f'INSERT INTO locations (location) '
                                 f'VALUES ("{location}")')
                db.commit()
                self.update_locations()
            if self.location_dialog.edit_radio.isChecked():
                editedlocation = self.location_dialog.locations_list.selectedItems()[0].text()
                mycursor.execute(f'SELECT location_id FROM locations '
                                 f'WHERE location="{editedlocation}"')
                location_id = mycursor.fetchall()[0][0]
                mycursor.execute(f'UPDATE locations '
                                 f'SET location="{location}" '
                                 f'WHERE location_id={location_id}')
                db.commit()
                self.update_locations()
        self.location_dialog.location_lineedit.clear()


if __name__ == '__main__':
    app = qtw.QApplication([])
    widget = LibraryWindow()
    widget.show()
    app.exec()
