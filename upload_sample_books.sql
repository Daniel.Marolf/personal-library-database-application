INSERT INTO keywords (keyword)
VALUES ("Fiction"), ("Biography"), ("Non-Fiction"), ("Childrens"), ("Classic"), ("History"), ("Ancient"), ("Religious"), ("Philosophy"), ("Parenting"), ("Cooking");

INSERT INTO authors (last_name, first_name)
VALUES ("McConaughey", "Matthew"), ("Watters", "Jesse"), ("Trejo", "Danny"), ("Logue", "Donal"), ("Lewis", "John"), ("Sehgal", "Kabir"), ("Young", "Andrew"), ("Obama", "Michelle"),
("Bland", "Nick"), ("Purtill", "Sharon"), ("Evans", "Rachel Held"), ("Turner", "Matthew Paul"), ("Cook", "Julia"), ("Spires", "Ashley"), ("Orwell", "George"), ("Reid", "Taylor Jenkins"),
("Lee", "Harper"), ("Austen", "Jane"), ("Golding", "William"), ("Steinbeck", "John"), ("Twain", "Mark"), ("Tolstoy", "Leo"), ("Rey", "H.A."), ("Rey", "Margaret"), ("Tzu", "Sun"),
("Mansfield", "Stephen"), ("Carnegie", "Dale"), ("Bronte", "Charlotte"), ("Wilkerson", "Isabel"), ("Coates", "Ta-Nehisi"), ("Noah", "Trevor"), ("Gray", "Deborah"), ("Joiner", "Reggie"),
("Nieuwhof", "Carey"), ("Page", "Karen"), ("Dornenburg", "Andrew"), ("Rowling", "J.K."), ("Doyle", "Glennon"), ("Winfrey", "Oprah"), ("Perry", "Bruce"), ("Brown", "Brene"), ("Diangelo", "Robin"),
("Foster", "Thomas");

INSERT INTO editors (last_name, first_name)
VALUES ("Carruthers", "Bob"), ("Kendi", "Ibram X."), ("Blain", "Keisha"), ("Wong", "Alice"), ("Leavis", "Q.D."), ("Trilling", "Lionel"), ("Hamilton", "Mark"), ("Cukrowski", "Kenneth"),
("Shankle", "Nancy"), ("Thompson", "James"), ("McKeon", "Richard");

INSERT INTO locations (location)
VALUES ("Home"), ("School"), ("Work"), ("Loaned To Mom");

INSERT INTO books (title, has_author, has_editor, location_id)
VALUES ("Greenlights", 1, 0, 1), ("How I Saved The World", 1, 0, 1), ("Trejo: My Life Of Crime, Redemption, And Hollywood", 1, 0, 1), ("Carry On: Reflections For A New Generation", 1, 0, 1),
("Becoming", 1, 0, 1), ("The Very Cranky Bear", 1, 0, 1), ("It'S Ok To Be Different", 1, 0, 1), ("What Is God Like?", 1, 0, 1), ("My Mouth Is A Volcano", 1, 0, 1), ("The Thing Lou Couldn'T Do", 1, 0, 1),
("1984", 1, 0, 1), ("Malibu Rising", 1, 0, 1), ("To Kill A Mockingbird", 1, 0, 1), ("Pride And Prejudice", 1, 0, 1), ("Lord Of The Flies", 1, 0, 1), ("Of Mice And Men", 1, 0, 1),
("The Adventures Of Huckleberry Finn", 1, 0, 1), ("Animal Farm", 1, 0, 1), ("War And Peace", 1, 0, 1), ("The Death Of Ivan Ilyich", 1, 0, 1), ("Anna Karenina", 1, 0, 1),
("Curious George Flies A Kite", 1, 0, 1), ("Curious George'S Dream", 1, 0, 1), ("Curious George Goes To The Hospital", 1, 0, 1), ("Curious George And The Firefighters", 1, 0, 1),
("The Art Of War", 1, 1, 1), ("Four Hundred Souls", 0, 1, 1), ("Disability Visability", 0, 1, 1), ("The Search For God And Guinness: A Biography Of The Beer That Changed The World", 1, 0, 1),
("How To Win Friends And Deal With People", 1, 0, 1), ("Jane Eyre", 1, 1, 1), ("Emma", 1, 1, 1), ("The Transforming Word", 0, 1, 1), ("Introduction To Aristotle", 0, 1, 1),
("The Warmth Of Other Suns: The Epic Story Of America'S Great Migration", 1, 0, 1), ("Between The World And Me", 1, 0, 1), ("Born A Crime: Stories From A South African Childhood", 1, 0, 1),
("Attaching In Adoption: Practical Tools For Today'S Parents", 1, 0, 1), ("Parenting Beyond Your Capacity", 1, 0, 1), ("The Flavor Bible", 1, 0, 1), ("Harry Potter And The Sorcerer'S Stone", 1, 0, 1),
("Harry Potter And The Chamber Of Secrets", 1, 0, 1), ("Harry Potter And The Prisoner Of Azkaban", 1, 0, 1), ("Harry Potter And The Goblet Of Fire", 1, 0, 1), ("Harry Potter And The Order Of The Phoenix", 1, 0, 1),
("Harry Potter And The Half-Blood Prince", 1, 0, 1), ("Harry Potter And The Deathly Hallows", 1, 0, 1), ("Untamed", 1, 0, 1), ("What Happened To You?: Conversations On Trauma, Resilience, And Healing", 1, 0, 1),
("The Gifts Of Imperfection", 1, 0, 1), ("White Fragility", 1, 0, 1), ("How To Read Literature Like A Professor", 1, 0, 1);

INSERT INTO authors_books (book_id, author_id)
VALUES (1,1), (2,2), (3,3), (3,4), (4,5), (4,6), (4,7), (5,8), (6,9), (7,10), (8,11), (8,12), (9,13), (10,14), 
(11,15), (12,16), (13,17), (14,18), (15,19), (16,20), (17,21), (18,15), (19,22), (20,22), (21,22), (22,23), (22,24), (23,23),
(23,24), (24,23), (24,24), (25,23), (25,24), (26,25), (29,26), (30,27), (31,28), (32,18), (35,29), (36,30), (37,31), (38,32),
(39,33), (39,34), (40,35), (40,36), (41,37), (42,37), (43,37), (44,37), (45,37), (46,37), (47,37), (48,38), (49,39), (49,40),
(50,41), (51,42), (52,43); 
-- (,), (,), (,), (,), (,), (,), (,), (,), (,), (,), (,), (,), (,), (,),

INSERT INTO editors_books (book_id, editor_id)
VALUES (26,1), (27,2), (27,3), (28,4), (31,5), (32,6), (33,7), (33,8), (33,9), (33,10), (34,11);
-- (,), (,), (,), (,), (,), (,), (,), (,), (,), (,), (,), (,), (,), (,),

INSERT INTO keywords_books (book_id, keyword_id)
VALUES (1,2), (1,3), (2,2), (2,3), (3,2), (3,3), (4,2), (4,3), (5,2), (5,3), (6,4), (7,4), (8,4), (8,8),
(9,4), (10,4), (11,1), (11,5), (12,1), (13,1), (13,5), (14,1), (14,5), (15,1), (15,5), (16,1), (16,5), (17,1),
(17,5), (18,1), (18,5), (19,1), (19,5), (20,1), (20,5), (21,1), (21,5), (22,4), (23,4), (24,4), (25,4), (26,5),
(26,7), (26,3), (27,3), (27,6), (28,3), (29,2), (29,3), (30,3), (31,1), (31,5), (32,1), (32,5), (33,8), (34,9),
(35,3), (35,6), (36,3), (37,2), (37,3), (38,3), (38,10), (39,3), (39,10), (40,3), (40,11), (41,1), (42,1), (43,1),
(44,1), (45,1), (46,1), (47,1), (48,3), (49,3), (50,3), (51,3), (52,3);
-- (,), (,), (,), (,), (,), (,), (,), (,), (,), (,), (,), (,), (,), (,),